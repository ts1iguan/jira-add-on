// database connection
var mysql = require('mysql');
var config = require('./config');

var con = mysql.createConnection({
    host: config.host,
    user: config.username,
    password: config.password,
    database: config.database
});
con.connect(function(err) {
    if(err) throw err;
    console.log('Database connected.')
});

module.exports.connection = con;