const jwt = require('atlassian-jwt');
const moment = require('moment');

function createToken(addon_data) {
    // get current time
    var now = moment.utc();

    // request type for query hash
    var token_req = {
        method: 'POST',
        originalUrl: 'rest/api/2/issues'
    };

    // setup token claims
    var token_config = {
        "iss": addon_data.addonKey,
        "iat": now.unix(),
        "exp": now.add(3, 'minutes').unix(),
        "qsh": jwt.createQueryStringHash(token_req)
    };

    // encode token
    return jwt.encode(token_config, addon_data.sharedSecret, 'HS256');
}

module.exports.createToken = createToken;