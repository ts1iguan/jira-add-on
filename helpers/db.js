const db = require('./../config/database').connection;

module.exports = {
    truncate: function (table) {
        return new Promise(function (resolve, reject) {
            var query = 'TRUNCATE '+table;
            db.query(query, function (err, results) {
                if(err) reject(err);
                resolve(results);
            });
        });
    }
};