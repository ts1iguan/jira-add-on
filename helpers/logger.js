const Log = require('log');
const fs = require('fs');

function log(data, type) {
    if(!type) type = 'info';
    var file = '';
    switch(type) {
        case 'debug': file = 'logs'; break;
        case 'error': file = 'errors'; break;
        case 'info': file = 'worklog'; break;
    }

    var stream = fs.createWriteStream(__dirname + '/../logs/'+file+'.log');
    const logger = new Log('debug', stream);
    logger[type](data);
    stream.close();
}

exports.log = log;