export interface Issue {
    key: string;
    type: string;
    summary: string;
    description: string;
}