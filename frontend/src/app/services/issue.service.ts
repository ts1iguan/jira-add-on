import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class IssueService {

  constructor(private http: Http) { }

  getIssues() {
    return this.http.get('/issues').map(
        result => result.json(),
        error => console.log(error)
    );
  }

  getIssue(id: string): Observable<any> {
    return this.http.get('/issues/'+id).map(
        result => result.json(),
        error => console.log(error)
    );
  }

  createIssue(issue: object): Observable<any> {
    return this.http.post('/issues', issue).map(
        result => result.json(),
        error => console.log(error)
    );
  }

  updateIssue(issue: object): Observable<any> {
      return this.http.put('/issues', issue).map(
          result => result.json(),
          error => console.log(error)
      );
  }

  deleteIssue(id: string): Observable<any> {
      return this.http.delete('/issues/'+id).map(
          result => result.json(),
          error => console.log(error)
      );
  }

  loadProjects() {
    return this.http.get('/projects').map(
        result => result.json(),
        error => console.log(error)
    );
  }

  loadProjectIssues(project_key: string) {
      return this.http.get('/projects/issues/'+project_key).map(
          result => result.json(),
          error => console.log(error)
      );
  }

}
