import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class SubTaskService {

    constructor(private http: Http) {
    }

    getSubTaskNames(): Observable<any> {
        return this.http.get('/sub-tasks').map(
            result => result.json(),
            error => console.log(error)
        );
    }

    saveSubTaskName(data: object): Observable<any> {
        return this.http.post('/sub-tasks', data).map(
            result => result.json(),
            error => console.log(error)
        );
    }

}
