import { TestBed, inject } from '@angular/core/testing';

import { SubTaskService } from './sub-task.service';

describe('SubTaskService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubTaskService]
    });
  });

  it('should be created', inject([SubTaskService], (service: SubTaskService) => {
    expect(service).toBeTruthy();
  }));
});
