import { RouterModule, Routes } from '@angular/router';

// components
import { HomeComponent } from './components/home/home.component';
import { CreateComponent } from './components/issues/create/create.component';
import { SubTaskFormComponent } from './components/sub-task-form/sub-task-form.component';

// routing
const appRoutes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'issues/create', component: CreateComponent, pathMatch: 'full' },
    { path: 'issues/create/:type', component: CreateComponent, pathMatch: 'full' },
    { path: 'issues/edit/:id', component: CreateComponent, pathMatch: 'full' },
    { path: 'issues/edit/:type/:id', component: CreateComponent, pathMatch: 'full' },
    { path: 'subtasks/create', component: SubTaskFormComponent, pathMatch: 'full' },
];

export const AppRouting = RouterModule.forRoot(appRoutes);
