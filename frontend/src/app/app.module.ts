import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import {APP_BASE_HREF} from '@angular/common';

// routers
import { AppRouting } from './app.routing';

// services
import { IssueService } from './services/issue.service';
import { SubTaskService } from './services/sub-task.service';

// components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { CreateComponent } from './components/issues/create/create.component';
import { SubTaskFormComponent } from './components/sub-task-form/sub-task-form.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreateComponent,
    SubTaskFormComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AppRouting
  ],
  providers: [
      IssueService,
      SubTaskService,
      [{provide: APP_BASE_HREF, useValue: '/create-issue'}]
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
