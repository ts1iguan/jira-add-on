import {Component, OnInit} from '@angular/core';
import {IssueService} from '../../services/issue.service';
import {SubTaskService} from '../../services/sub-task.service';
import {Router} from '@angular/router';
import {NgForm} from "@angular/forms";

@Component({
    selector: 'app-sub-task-form',
    templateUrl: './sub-task-form.component.html',
    styleUrls: ['./sub-task-form.component.css']
})
export class SubTaskFormComponent implements OnInit {

    private projects = [];
    private subtasks = [];
    private model: object = {
        project_key: '',
        name: ''
    };
    private loading = true;

    constructor(private issueService: IssueService,
                private subTaskService: SubTaskService,
                private router: Router) {
    }

    ngOnInit() {
        this.loadProjects();
        this.loadSubTasks();
    }

    loadProjects() {
        this.issueService.loadProjects().subscribe((result) => {
            if (result.success) {
                this.projects = result.projects;
                this.loading = false;
            }
        });
    }

    loadSubTasks() {
        this.subTaskService.getSubTaskNames().subscribe((result) => {
            if (result.success) {
                this.subtasks = result.subtasks;
            }
        });
    }

    submit(form: NgForm) {
        this.subTaskService.saveSubTaskName(this.model).subscribe((result) => {
            if (result.success) {
                this.loadSubTasks();
                this.model = {
                    project_key: '',
                    name: ''
                };
                form.reset();
            }
        });
    }


}
