import { Component, OnInit } from '@angular/core';
import { IssueService } from '../../services/issue.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private issues = [];
  private loading: boolean = true;

  constructor(private issueService: IssueService) { }

  ngOnInit() {
      this.getIssues();
  }

  getIssues() {
    this.issueService.getIssues().subscribe(result => {
      if(result.success) {
        this.issues = result.issues;
        this.loading = false;
      }
    });
  }

  deleteIssue(key: string, index: number) {
    this.issueService.deleteIssue(key).subscribe(result => {
        if(result.success) {
            this.issues.splice(index, 1);
        }
      }
    );
  }

}
