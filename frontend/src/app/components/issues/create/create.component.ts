import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Issue } from '../../../models/issue';
import { IssueService } from '../../../services/issue.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  private model: object = {
      key: '',
      issue_key: '',
      type: 'Task',
      summary: '',
      description: ''
  };
  private loading: boolean = false;
  private formLoaded: boolean = false;
  private issue_types = [
      'Task', 'Sub-task', 'Epic', 'Story', 'Bug'
  ];
  private is_update: boolean = false;
  private projects: object = [];
  private issues: object = [];
  private task_type: string = 'Task';


  constructor(private issueService: IssueService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
      this.loadIssue();
  }

  translations(type: string) {
    const issue_types = {
        'Задача' : 'Task',
        'Подзадача' : 'Sub-task',
        'История' : 'Story',
        'Ошибка' : 'Bug',
        'Epic' : 'Epic',
    };

    return issue_types[type];
  }

  submit() {
    this.loading = true;
    let method = this.is_update ? 'updateIssue' : 'createIssue';
    this.model['type'] = this.task_type;
    this.issueService[method](this.model).subscribe(result => {
      this.loading = false;
      if(result.success) this.router.navigate(['']);
      else alert(result.message);
    });
  }

  loadIssue() {
      this.route.params.subscribe((params: any) => {
          if(params.type && params.type === 'sub-task') {
              this.task_type = 'Sub-task';
          }

          if(params.id) {
              this.issueService.getIssue(params.id).subscribe(result => {
                  if(result.success) {
                      let issue = result.issue;
                      let issue_type = (this.issue_types.indexOf(issue.fields.issuetype.name) < 0) ?
                          this.translations(issue.fields.issuetype.name) : issue.fields.issuetype.name;
                      this.model = {
                          key: issue.key,
                          type: issue_type,
                          summary: issue.fields.summary,
                          description: issue.fields.description
                      };
                      this.is_update = true;
                  }
                  this.loadProjects();
              });
          } else {
              this.loadProjects();
          }
      });
  }

  loadProjects() {
      this.issueService.loadProjects().subscribe((result) => {
          if(result.success) {
              this.projects = result.projects;
              this.formLoaded = true;
          }
      });
  }

  loadProjectIssues(project_key: string) {
      if(this.task_type !== 'Sub-task') return false;
      this.issueService.loadProjectIssues(project_key).subscribe((result) => {
          if(result.success && result.issues.issues.length) {
              this.issues = result.issues.issues;
              this.model['issue_key'] = this.issues[0].key;
          }
      });
  }


}
