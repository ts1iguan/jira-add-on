const db = require('./../config/database').connection;
const helper = require('./../helpers/db');
const logger = require('./../helpers/logger');

module.exports = {
    loadConfig: function (next) {
        var query = 'SELECT * FROM addon_data WHERE id = 1';
        db.query(query, function(err, results) {
            if(err) reject(err);
            next(results[0]);
        });
    },
    saveConfig: function (addon_config) {
        helper.truncate('addon_data').then(function() {
            var query = 'INSERT INTO addon_data (addonKey, clientKey, publicKey, sharedSecret, baseUrl, productType, description) ' +
                'VALUES ("'+addon_config.key+'", "'+addon_config.clientKey+'", "'+addon_config.publicKey+'", "'+addon_config.sharedSecret+'", ' +
                '"'+addon_config.baseUrl+'", "'+addon_config.productType+'", "'+addon_config.description+'");';
            db.query(query, function(err) {
               if(err) logger.log(err, 'error');
            });
        });
    }
};