const db = require('./../config/database').connection;

module.exports = {
    getIssues: function() {
        return new Promise(function (resolve, reject) {
            var query = 'SELECT * FROM issues ORDER by id DESC';
            db.query(query, function(err, results) {
                if(err) reject(err);
                resolve(results);
            });
        });
    },
    saveIssue: function (issue, response) {
        return new Promise(function (resolve, reject) {
            if(!issue.issue_key) issue['issue_key'] = '';
            var query = 'INSERT INTO issues (issue_key, type, project_key, parent_key, summary, description, created_at)' +
                ' VALUES("'+response.key+'", "'+issue.type+'", "'+issue.key+'",  "'+issue.issue_key+'", "'+issue.summary+'", "'+issue.description+'", "2017-11-20 15:00:00")';

            db.query(query, function (err, results) {
                if(err) reject(err);
                resolve(results);
            });
        });
    }
};