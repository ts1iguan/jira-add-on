const db = require('./../config/database').connection;

module.exports = {
    getSubtasks: function () {
        return new Promise(function (resolve, reject) {
            var query = 'SELECT * FROM sub_tasks ORDER by id DESC';
            db.query(query, function(err, results) {
                if(err) reject(err);
                resolve(results);
            });
        });
    },

    getSubTaskByProjectKey: function (project_key) {
        return new Promise(function (resolve, reject) {
            var query = 'SELECT * FROM sub_tasks WHERE project_key = "'+project_key+'"';
            db.query(query, function (err, results) {
                if(err) reject(err);
                resolve(results);
            });
        });
    },

    saveSubTaskName: function (subtask) {
        return new Promise(function (resolve, reject) {
            var query = 'SELECT id FROM sub_tasks WHERE project_key = "'+subtask.project_key+'"', save_query;
            db.query(query, function (err, results) {
                if(err) reject(err);
                if(results.length > 0) {
                    save_query = 'UPDATE sub_tasks SET name = "'+subtask.name+'" WHERE id = '+results[0].id;
                    db.query(save_query, function (err, save_results) {
                        if(err) reject(err);
                        resolve(results[0].id);
                    });
                } else {
                    save_query = 'INSERT INTO sub_tasks (project_key, name) VALUES("'+subtask.project_key+'",  "'+subtask.name+'")';
                    db.query(save_query, function (err, save_results) {
                        if(err) reject(err);
                        resolve(0);
                    });
                }
            });
        });
    }
};