module.exports = function (app, addon) {

    var client;
    const logger = require('./../helpers/logger');
    const issue = require('./../models/issue');
    const subtask = require('./../models/subtask');
    const addon_config = require('./../models/addon_config');


    // Root route. This route will serve the `atlassian-connect.json` unless the
    // documentation url inside `atlassian-connect.json` is set
    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    // add-on starting point
    app.get('/create-issue', addon.authenticate(), function(req, res) {
        client = addon.httpClient(req);
        res.render('home');
    });

    // store add-on data on installation
    app.post('/installed', function (req, res, next) {
        addon_config.saveConfig(req.body);
        next();
    });

    // load all issues from API
    app.get('/issues', function (req, res) {
        issue.getIssues(res).then(function (results) {
            res.contentType('application/json');
            res.send({
                success: true,
                issues: results
            });
        }).catch(function(err) {
            throw err;
        });


        // client.get({
        //     url: '/rest/api/2/search'
        // }, function (err, apiResponse, body) {
        //     if (err) throw err;
        //     else {
        //         if (body && body.length > 0) {
        //             var issues = [],
        //                 addon_name = helper.getAddonName(),
        //                 results = JSON.parse(body);
        //
        //             results.issues.map(function (issue) {
        //                 if (issue.fields.creator.key = 'addon_' + addon_name) {
        //                     issues.push(issue);
        //                 }
        //             });
        //             res.contentType('application/json');
        //             res.send({
        //                 success: true,
        //                 issues: issues
        //             });
        //         }
        //     }
        // });

    });

    // get single issue by ID
    app.get('/issues/:id', function (req, res) {
        if(!req.params.id) {
            throw 'Invalid ID';
        }
        client.get({
            url: '/rest/api/2/issue/'+req.params.id,
            headers: {'X-Atlassian-Token': 'nocheck'}
        }, function (err, clientResponse, body) {
            if(err) throw err;
            res.contentType('application/json');
            res.send({
                success: true,
                issue: body
            });
        });
    });

    // create new issue request
    app.post('/issues', function (req, res) {
        var data = {
            "fields": {
                "project": {
                    "key": req.body.key
                },
                "summary": req.body.summary,
                "description": req.body.description,
                "issuetype": {
                    "name": req.body.type
                }
            }
        };

        if(req.body.type === 'Sub-task' && req.body.issue_key) {
            data.fields['parent'] = { key: req.body.issue_key };
        }

        client.post({
            url: '/rest/api/2/issue',
            headers: {
                'X-Atlassian-Token': 'nocheck'
            },
            body: data,
            json: true
        }, function (err, httpResp, body) {
            if(err) throw err;
            issue.saveIssue(req.body, body)
                .then(function() {
                    res.contentType('application/json');
                    res.status(200);
                    res.send({
                        success: true,
                        issue: body
                    });
                })
                .catch(function(err) {
                throw err;
            });
        });
    });

    // update issue request
    app.put('/issues/:id', function (req, res) {
        var issue_id = req.params.id;
        if(!issue_id) throw 'Invalid ID';
        var data = {
            "fields": {
                "summary": req.body.summary,
                "description": req.body.description
            }
        };
        client.put({
            url: '/rest/api/2/issue/'+issue_id,
            headers: {
                'X-Atlassian-Token': 'nocheck'
            },
            body: data,
            json: true
        }, function (err, httpResp, body) {
            if(err) throw err;
            res.contentType('application/json');
            res.send({
                success: true,
                issue: body
            });
        });
    });

    // delete issue request
    app.delete('/issues/:id', function (req, res) {
        var issue_id = req.params.id;
        if (!issue_id) throw 'Invalid ID';

        client.post({
            _method: 'DELETE',
            url: '/rest/api/2/issue/' + issue_id,
            headers: {
                'X-Atlassian-Token': 'nocheck'
            }
        }, function (err, httpResp, body) {
            if (err) throw err;
            res.contentType('application/json');
            res.status(200);
            res.send({
                success: true,
                response: body,
                code: httpResp.statusCode
            });
        });
    });

    app.get('/projects', function (req, res) {
        client.get({
            url : '/rest/api/2/project'
        }, function (err, httpResp, body) {
            if(err) throw err;
            res.contentType('application/json');
            res.send({
                success: true,
                projects: JSON.parse(body)
            });
        });
    });

    app.get('/projects/issues/:project_key', function (req, res) {
        if(!req.params.project_key) throw 'Invalid project key';
        client.get({
            url : '/rest/api/2/search?jql=project='+req.params.project_key+'+order+by+duedate&fields=id,key,fields'
        }, function (err, httpResp, body) {
            if(err) throw err;
            res.contentType('application/json');
            res.send({
                success: true,
                issues: JSON.parse(body)
            });
        });
    });

    app.get('/sub-tasks', function (req, res) {
        subtask.getSubtasks().then(function (results) {
            res.contentType('application/json');
            res.send({
                success: true,
                subtasks: results
            });
        }).catch(function(err) {
            throw err;
        });
    });

    app.post('/sub-tasks', function (req, res) {
        subtask.saveSubTaskName(req.body).then(function(results) {
            res.contentType('application/json');
            res.send({
                success: true,
                subtasks: req.body,
                id: results
            });
        });
    });

    // webhook: create issue event
    app.post('/webhook/issue', addon.authenticate(), function (req, res) {
        var event = req.body;
        var issueData = event.issue;
        if(issueData.fields.issuetype.name !== 'Task') {
            logger.log('Not an issue');
            return false;
        }

        subtask.getSubTaskByProjectKey(issueData.fields.project.key).then(function (results) {
            if(results.length > 0) {
                var subtask_name = results[0].name;
                var data = {
                    key: issueData.fields.project.key,
                    type: 'Sub-task',
                    issue_key: issueData.key,
                    summary: subtask_name,
                    description: 'Test sub-task for issue:'+issueData.key
                };

                var api_data = {
                    "fields": {
                        "project": {
                            "key": issueData.fields.project.key
                        },
                        "parent": {
                            "key": issueData.key
                        },
                        "summary": subtask_name,
                        "description": 'Test sub-task for issue: '+issueData.key,
                        "issuetype": {
                            "name": 'Sub-task'
                        }
                    }
                };

                var webhookClient = addon.httpClient(req);
                webhookClient.post({
                    url: '/rest/api/2/issue',
                    headers: {
                        'X-Atlassian-Token': 'nocheck'
                    },
                    body: api_data,
                    json: true
                }, function (err, httpResp, body) {
                    if (err) throw err;
                    issue.saveIssue(data, body)
                        .then(function () {
                            logger.log(body, 'info');
                            res.sendStatus(200);
                        })
                        .catch(function (err) {
                            logger.log(err, 'error');
                        });
                });
            }
        });
    });


    // load any additional files you have in routes and apply those to the app
    {
        var fs = require('fs');
        var path = require('path');
        var files = fs.readdirSync("routes");
        for(var index in files) {
            var file = files[index];
            if (file === "index.js") continue;
            // skip non-javascript files
            if (path.extname(file) != ".js") continue;

            var routes = require("./" + path.basename(file));

            if (typeof routes === "function") {
                routes(app, addon);
            }
        }
    }
};
