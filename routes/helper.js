const connect = require('./../atlassian-connect.json');
const fs = require('fs');
const Log = require('log');

function getAddonName() {
    return connect.key;
}

function log(data) {
    const logger = new Log('debug', fs.createWriteStream('./logs/log.log'));
    logger.info(data);
}

module.exports = {
    getAddonName: getAddonName,
    log: log
};